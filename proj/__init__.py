from flask import Flask

from proj.pub_quiz.quiz import quiz
from proj.top_trumps.top_trumps import top_trumps


def create_app():
    """Create the flask app"""
    app = Flask(__name__)

    app.register_blueprint(top_trumps)
    app.register_blueprint(quiz)

    return app
