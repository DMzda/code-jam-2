import json


from flask import Blueprint, jsonify

quiz = Blueprint("quiz", __name__, url_prefix="/quiz")


@quiz.route("/pubquiz")
def all_questions():
    with quiz.open_resource("questions.json") as question_file:
        questions = json.load(question_file)

    return jsonify(questions)
