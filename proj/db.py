import rethinkdb as r
from rethinkdb import ReqlRuntimeError


def setup_db():
    with r.connect(host="localhost", port=28015, db="top_trumps") as rdb_connection:
        try:
            r.db_drop("top_trumps").run(rdb_connection)
        except ReqlRuntimeError:
            pass

        try:
            r.db_create("top_trumps").run(rdb_connection)

            r.table_create("players").run(rdb_connection)
            r.db("top_trumps").table("players").insert({"name": "CPU"}).run(rdb_connection)

            r.table_create("games").run(rdb_connection)
        except ReqlRuntimeError:
            pass
