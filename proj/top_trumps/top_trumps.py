import json
import random

import rethinkdb as r
from flask import Blueprint, abort, g, jsonify, request
from rethinkdb import ReqlDriverError

top_trumps = Blueprint("top_trumps", __name__, url_prefix="/top_trumps")


@top_trumps.before_request
def before_request():
    try:
        g.rdb_connection = r.connect(host="localhost", port=28015, db="top_trumps")
    except ReqlDriverError:
        abort(503, "Database connection could not be established")


@top_trumps.teardown_request
def teardown_request(exception):
    try:
        g.rdb_connection.close()
    except AttributeError:
        pass


@top_trumps.route("/cards")
def all_cards():
    with top_trumps.open_resource("cards.json") as card_file:
        cards = json.load(card_file)

    return jsonify(cards)


@top_trumps.route("/cards/<int:card_id>")
def card_details(card_id):
    with top_trumps.open_resource("cards.json") as card_file:
        cards = json.load(card_file)

    for card in cards["cards"]:
        if card["id"] == card_id:
            return jsonify(card)

    return jsonify(error=f"No card with id {card_id}")


@top_trumps.route("/players", methods=["GET", "POST"])
def players():
    if request.method == "POST":
        json_data = request.get_json()
        if not json_data:
            return jsonify(error="No data sent")

        name = json_data.get("name", "")
        if not name:
            return jsonify(error="No name given")
        elif r.db("top_trumps").table("players")["name"].contains(name).run(g.rdb_connection):
            return jsonify(error=f"{name} already exists")

        new_player = r.db("top_trumps").table("players").insert({"name": name}).run(g.rdb_connection)
        return jsonify(id=new_player["generated_keys"][0], name=name)

    all_players = list(r.db("top_trumps").table("players").run(g.rdb_connection))
    return jsonify(players=all_players)


@top_trumps.route("/players/<player_id>")
def get_player(player_id):
    player = r.db("top_trumps").table("players").get(player_id).run(g.rdb_connection)

    if not player:
        return jsonify(error=f"No player with id {player_id} found")

    return jsonify(player)


def start_game(player_ids):
    """Start a new game"""
    player_count = len(player_ids)
    if player_count == 1:
        cpu = next(r.db("top_trumps").table("players").filter({"name": "CPU"}).run(g.rdb_connection))
        player_ids.append(cpu["id"])
        player_count += 1

    status = "running"

    with top_trumps.open_resource("cards.json") as card_file:
        cards = json.load(card_file)["cards"]

    players = list(r.db("top_trumps").table("players").get_all(*player_ids).run(g.rdb_connection))

    player_cards = [{"id": player, "cards": []} for player in player_ids]

    random.shuffle(cards)
    for i in range(len(cards)):
        player_cards[i % player_count]["cards"].append(cards.pop())

    next_player = r.db("top_trumps").table("players").get(player_ids[0]).run(g.rdb_connection)

    game = {"player_count": player_count, "players": players, "next_turn": next_player, "status": status,
            "state": {"players": player_cards, "middle": []}}
    new_game = r.db("top_trumps").table("games").insert(game).run(g.rdb_connection)

    return new_game["generated_keys"][0]


@top_trumps.route("/games", methods=["GET", "POST"])
def games():
    if request.method == "POST":
        json_data = request.get_json()
        if not json_data:
            return jsonify(error="No data sent")

        game_id = start_game(json_data["players"])
        return jsonify(r.db("top_trumps").table("games").get(game_id).run(g.rdb_connection))

    all_games = list(r.db("top_trumps").table("games").run(g.rdb_connection))
    return jsonify(games=all_games)


def take_turn(game_id, choice):
    game = r.db("top_trumps").table("games").get(game_id).run(g.rdb_connection)

    next_player = r.db("top_trumps").table("players").get(game["next_turn"]["id"]).run(g.rdb_connection)
    top_cards = [{"player_id": player["id"], "card": player["cards"].pop()} for player in game["state"]["players"] if
                 player["cards"]]
    # top_cards.extend(game["state"]["middle"])
    #
    # game["state"]["middle"] = []

    for index, card in enumerate(top_cards):
        if card["player_id"] == next_player["id"]:
            to_remove = index

    current_winner = top_cards.pop(to_remove)
    players_card = current_winner["card"]

    for card in top_cards:
        if card["card"][choice] > current_winner["card"][choice]:
            current_winner = card

    to_give = [card["card"] for card in top_cards]
    to_give.append(players_card)
    random.shuffle(to_give)

    next_player = r.db("top_trumps").table("players").get(current_winner["player_id"]).run(g.rdb_connection)

    for index, player in enumerate(game["state"]["players"]):
        if player["id"] == next_player["id"]:
            for card in to_give:
                game["state"]["players"][index]["cards"].insert(0, card)

    game["next_turn"] = next_player
    r.db("top_trumps").table("games").get(game_id).update(game).run(g.rdb_connection)

    return game


def check_winner(game_id):
    game = r.db("top_trumps").table("games").get(game_id).run(g.rdb_connection)

    in_game = []
    for player in game["state"]["players"]:
        if len(player["cards"]) != 0:
            in_game.append(player)

    if len(in_game) == 1:
        game["status"] = "ended"
        game["winner"] = in_game[0]["id"]
        r.db("top_trumps").table("games").get(game_id).update(game).run(g.rdb_connection)

        return r.db("top_trumps").table("players").get(in_game[0]["id"]).run(g.rdb_connection)

    return None


@top_trumps.route("/games/<game_id>", methods=["GET", "POST"])
def play_game(game_id):
    if request.method == "POST":
        json_data = request.get_json()
        if not json_data:
            return jsonify(error="No data sent")

        # No choice needed if CPU turn
        choice = json_data.get("choice", None)

        turn = take_turn(game_id, choice)
        winner = check_winner(game_id)
        if winner:
            return jsonify(winner=winner)

        return jsonify(turn)

    game = r.db("top_trumps").table("games").get(game_id).run(g.rdb_connection)

    if not game:
        return jsonify(error=f"No game with id {game} found")

    return jsonify(game)
