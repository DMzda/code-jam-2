# Code Jam 2: Mythology API

## [Team Needy Glasses](https://pythondiscord.com/jams/team/c1b7fd9a-8121-4c35-915f-9150a581d9c2)

We decided to create a game of top trumps using mythological creatures.

## Setup

1. Install [rethinkdb](https://www.rethinkdb.com/docs/install/)
2. Install the python requirements (preferably in a virtualenv):
    ```
    pip install -r requirements.txt
    ```
3. Set the `FLASK_APP` environmental variable to point to run.py
4. Run `flask setup_db` to setup the database
5. Use `flask run` to run the app

## Game flow

1. Create a new user:
    ```
    POST http://localhost:5000/top_trumps/players
    Content-Type: application/json
    
    {"name": "DMzda"}
    ```
    Response:
    ```json
    {
      "id": "5f5704b8-9143-4650-9aec-fe6e458166fd",
      "name": "DMzda"
    }
    ```
2. Start a game:
    ```
    POST http://localhost:5000/top_trumps/games
    Content-Type: application/json
    
    {"players": ["5f5704b8-9143-4650-9aec-fe6e458166fd"]}
    ```
    Response: The new game with it's full state
    ```json
    {
      "id": "a3073c54-8896-4592-85ae-054c0f2303d1",
      "next_turn": {
        "id": "5f5704b8-9143-4650-9aec-fe6e458166fd",
        "name": "DMzda"
      },
      "player_count": 2,
      "players": [
        {
          "id": "5f5704b8-9143-4650-9aec-fe6e458166fd",
          "name": "DMzda"
        },
        {
          "id": "283b5855-022d-4e54-b88e-d285d77aaf0c",
          "name": "CPU"
        }
      ],
      "state": {
        "middle": [],
        "players": [
          {
            "cards": [
              {
                "Bravery": 9,
                "Fear Factor": 5,
                "Ferocity": 3,
                "Strength": 40,
                "Wisdom": 80,
                "description": "This is...",
                "id": 10,
                "name": "Sisyphus"
              }, ...
            ],
            "id": "5f5704b8-9143-4650-9aec-fe6e458166fd"
          },
          {
            "cards": [
              {
                "Bravery": 20,
                "Fear Factor": 30,
                "Ferocity": 15,
                "Strength": 150,
                "Wisdom": 90,
                "description": "This is...",
                "id": 17,
                "name": "Poseidon"
              }, ...
            ],
            "id": "283b5855-022d-4e54-b88e-d285d77aaf0c"
          }
        ]
      },
      "status": "running"
    }

    ```
3. Take the turn for the player that has the `next_turn` by selecting a top trumps attribute. Note that the top card is 
the last card in the `cards` array.
    ```
    POST http://localhost:5000/top_trumps/games/a3073c54-8896-4592-85ae-054c0f2303d1
    Content-Type: application/json
    
    {"choice": "Bravery"}
    ``` 
    Response: The new game state as described above.
4. Continue taking turns for the player who has the `next_turn` until a winner is declared. Note that the CPU is not 
implemented, so just take a turn as usual.
    ```
    POST http://localhost:5000/top_trumps/games/a3073c54-8896-4592-85ae-054c0f2303d1
    Content-Type: application/json
    
    {"choice": "Bravery"}
    ```
    Response:
    ```json
    {
      "winner": {
        "id": "5f5704b8-9143-4650-9aec-fe6e458166fd",
        "name": "DMzda"
      }
    }
    ```

## All endpoints

* `/cards`: Returns a list of all of the cards
* `/cards/<card_id>`: Returns the details of a specific card
* `/players`: 
    * `GET`: Returns a list of all players
    * `POST`: Creates a new player
* `/players/<player_id>`: Returns the details of a specific player
* `/games`:
    * `GET`: Returns a list of all games
    * `POST`: Creates a new game
* `/games/<game_id>`:
    * `GET`: Returns the current state of the game
    * `POST`: Take a turn

## NotImplementedError

* Cleaner code
* Correct return codes and error handling
* Better documentation
* Tests
* A cli client
* CPU player(s)
* More cards
