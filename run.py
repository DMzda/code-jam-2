from proj import create_app, db

app = create_app()


@app.cli.command()
def setup_db():
    db.setup_db()
    print("Database setup complete.")


if __name__ == "__main__":
    app.run(debug=True)
